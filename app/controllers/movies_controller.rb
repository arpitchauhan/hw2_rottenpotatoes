class MoviesController < ApplicationController
			
  
  def show

    id = params[:id] # retrieve movie ID from URI route
    @movie = Movie.find(id) # look up movie by unique ID
    # will render app/views/movies/show.<extension> by default
  	
  end

  def index
	 #debugger
  	
    @all_ratings = Movie.all_ratings
    @all_ratings_hash = Hash.new
	 @all_ratings.each { |rating| @all_ratings_hash[rating] = 1 }
	 @checked ||= Hash.new
    
    if params.has_key?(:ratings) != true and session.has_key?(:ratings) == true 
   		params[:ratings] = session[:ratings]
    end
    
    if params[:sort_by] != nil
    
    	@movies = Movie.where("rating IN (:ratings)", :ratings => params.has_key?(:ratings) ? params[:ratings].keys : @all_ratings).find(:all, :order => params[:sort_by])
    	
=begin
 
      @movies = Movie.where("rating IN (:ratings)" , :ratings => params.has_key?(:ratings) ? params[:ratings].keys : (session.has_key?(:ratings) ? session[:ratings].keys : @all_ratings)).find(:all, :order => params[:sort_by])
=end
    
		elsif session[:sort_by] != nil
			
			flash.keep
				
			if params[:ratings] != nil  
				def query
					ratings_query = ""
					
					params[:ratings].each do |k, v|
						ratings_query << "&" << "ratings[#{k}]=#{v}"
					end
					ratings_query
				end					 
				redirect_to "/movies/?sort_by=#{session[:sort_by]}#{query}"
			elsif session[:ratings] != nil
				def query
					ratings_query = ""
					
					session[:ratings].each do |k, v|
						ratings_query << "&" << "ratings[#{k}]=#{v}"
					end
					ratings_query
				end
				redirect_to "/movies/?sort_by=#{session[:sort_by]}#{query}"
			else
				redirect_to "/movies/?sort_by=#{session[:sort_by]}"
			end 
						
			 
    else
    	@movies = Movie.where("rating IN (:ratings)", :ratings => params.has_key?(:ratings) ? params[:ratings].keys : @all_ratings)
    	
=begin
    	@movies = Movie.where("rating IN (:ratings)" , :ratings => params.has_key?(:ratings) ? params[:ratings].keys : (session.has_key?(:ratings) ? session[:ratings].keys : @all_ratings)) 
=end
    end
#=begin
		  
    @all_ratings.each do |rating|
				if params.has_key?(:ratings)
					if params[:ratings].keys.include?(rating)
						@checked[rating] = true 
					else
						@checked[rating] = false
					end
				else
    		  @checked[rating] = true
    	  end
    end
      #debugger
      if session.has_key?(:ratings)
      	session[:ratings] = params.has_key?(:ratings) && params[:submit] != 'Refresh' ? params[:ratings] : session[:ratings]
      else
		  
        session[:ratings] = params.has_key?(:ratings) ? params[:ratings] :  @all_ratings_hash
      end

      session[:sort_by] = params.has_key?(:sort_by) ? params[:sort_by] : (session.has_key?(:sort_by) ?  session[:sort_by] : nil) 
      #debugger
#=end    
  end

  def new
    # default: render 'new' template
  end

  def create
    @movie = Movie.create!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully created."
    redirect_to movies_path
  end

  def edit
    @movie = Movie.find params[:id]
  end

  def update
    @movie = Movie.find params[:id]
    @movie.update_attributes!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully updated."
    redirect_to movie_path(@movie)
  end

  def destroy
    @movie = Movie.find(params[:id])
    @movie.destroy
    flash[:notice] = "Movie '#{@movie.title}' deleted."
    redirect_to movies_path
  end

end
